//
//  SecondViewController.swift
//  MyFirstiOSApp
//
//  Created by Audrey BOUCHER on 2/13/16.
//  Copyright © 2016 aboucher. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class SecondViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    var manager: CLLocationManager!

    override func viewDidLoad() {
        super.viewDidLoad()

        let location = CLLocationCoordinate2DMake(48.8966838, 2.3183755)
        let span = MKCoordinateSpanMake(0.005, 0.005)
        let region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = "École 42"
        annotation.subtitle = "Paris"
        mapView.addAnnotation(annotation)
    }

    func barLocation(bar: Int) {
        print(bar)
        let location = CLLocationCoordinate2DMake(48.8966838, 2.3183755)
        let span = MKCoordinateSpanMake(0.005, 0.005)
        let region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = "École 42"
        annotation.subtitle = "Paris"
        mapView.addAnnotation(annotation)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func segmentedControlAction(sender: UISegmentedControl!) {
        if sender.selectedSegmentIndex == 0 {
            mapView.mapType = MKMapType.Standard
        }
        else if sender.selectedSegmentIndex == 1 {
            mapView.mapType = MKMapType.Hybrid
        }
        else if sender.selectedSegmentIndex == 2 {
            mapView.mapType = MKMapType.Satellite
        }
    }

    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005))
        self.mapView.setRegion(region, animated: true)
        self.manager.stopUpdatingLocation()
    }

    @IBAction func managerLocation(button: UIButton!){
        manager = CLLocationManager()

        self.manager.delegate = self
        self.manager.desiredAccuracy = kCLLocationAccuracyBest
        self.manager.requestWhenInUseAuthorization()
        self.manager.startUpdatingLocation()
        self.mapView.showsUserLocation = true
    }
}